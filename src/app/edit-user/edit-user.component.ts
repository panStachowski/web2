import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {AvatarDialogComponent} from '../avatar-dialog/avatar-dialog.component';
import {FirebaseService} from '../services/firebase.service';
import {Router} from '@angular/router';
import {Wealth} from '../new-user/new-user.component';

export interface Wealth {
    name: string;
    action: string;
}

export interface Damage {
    name: string;
    action: string;
}

@Component({
    selector: 'app-edit-user',
    templateUrl: './edit-user.component.html',
    styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

    @ViewChild('uploadResultPaginatorWealth', {read: MatPaginator}) uploadResultPaginatorWealth: MatPaginator;
    @ViewChild('uploadResultPaginatorDamage', {read: MatPaginator}) uploadResultPaginatorDamage: MatPaginator;

    exampleForm: FormGroup;
    item: any;
    panelOpenState = false;
    displayedColumns: string[] = ['name', 'action'];
    wealth = [];
    damage = [];
    dataSource: any;
    damageData: any;

    command = [];
    gamble = [];
    ride = [];
    drinking = [];
    animals = [];
    gossip = [];
    swim = [];
    drive = [];
    charm = [];
    search = [];
    creep = [];
    perception = [];
    survival = [];
    haggle = [];
    hiding = [];
    rowing = [];
    climbing = [];
    evaluate = [];
    intimidation = [];


    abbControl = {
        commandW: false,
        command10: false,
        command20: false,
        gambleW: false,
        gamble10: false,
        gamble20: false,
        rideW: false,
        ride10: false,
        ride20: false,
        drinkingW: false,
        drinking10: false,
        drinking20: false,
        animalsW: false,
        animals10: false,
        animals20: false,
        gossipW: false,
        gossip10: false,
        gossip20: false,
        swimW: false,
        swim10: false,
        swim20: false,
        driveW: false,
        drive10: false,
        drive20: false,
        charmW: false,
        charm10: false,
        charm20: false,
        searchW: false,
        search10: false,
        search20: false,
        creepW: false,
        creep10: false,
        creep20: false,
        perceptionW: false,
        perception10: false,
        perception20: false,
        survivalW: false,
        survival10: false,
        survival20: false,
        haggleW: false,
        haggle10: false,
        haggle20: false,
        hidingW: false,
        hiding10: false,
        hiding20: false,
        rowingW: false,
        rowing10: false,
        rowing20: false,
        climbingW: false,
        climbing10: false,
        climbing20: false,
        evaluateW: false,
        evaluate10: false,
        evaluate20: false,
        intimidationW: false,
        intimidation10: false,
        intimidation20: false,
    };


    boolCommand(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.commandW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.command10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.command20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.commandW = false;
            this.abbControl.command10 = false;
            this.abbControl.command20 = false;
        }
    }

    boolGamble(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.gambleW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.gamble10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.gamble20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.gambleW = false;
            this.abbControl.gamble10 = false;
            this.abbControl.gamble20 = false;
        }
    }
    boolRide(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.rideW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.ride10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.ride20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.rideW = false;
            this.abbControl.ride10 = false;
            this.abbControl.ride20 = false;
        }
    }

    boolDrikning(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.drinkingW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.drinking10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.drinking20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.drinkingW = false;
            this.abbControl.drinking10 = false;
            this.abbControl.drinking20 = false;
        }
    }

    boolAnimals(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.animalsW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.animals10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.animals20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.animalsW = false;
            this.abbControl.animals10 = false;
            this.abbControl.animals20 = false;
        }
    }

    boolGossip(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.gossipW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.gossip10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.gossip20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.gossipW = false;
            this.abbControl.gossip10 = false;
            this.abbControl.gossip20 = false;
        }
    }

    boolSwim(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.swimW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.swim10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.swim20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.swimW = false;
            this.abbControl.swim10 = false;
            this.abbControl.swim20 = false;
        }
    }

    boolDrive(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.driveW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.drive10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.drive20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.driveW = false;
            this.abbControl.drive10 = false;
            this.abbControl.drive20 = false;
        }
    }

    boolCharm(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.charmW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.charm10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.charm20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.charmW = false;
            this.abbControl.charm10 = false;
            this.abbControl.charm20 = false;
        }
    }

    boolSearch(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.searchW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.search10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.search20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.searchW = false;
            this.abbControl.search10 = false;
            this.abbControl.search20 = false;
        }
    }

    boolCreep(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.creepW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.creep10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.creep20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.creepW = false;
            this.abbControl.creep10 = false;
            this.abbControl.creep20 = false;
        }
    }

    boolPerception(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.perceptionW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.perception10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.perception20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.perceptionW = false;
            this.abbControl.perception10 = false;
            this.abbControl.perception20 = false;
        }
    }

    boolSurvival(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.survivalW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.survival10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.survival20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.survivalW = false;
            this.abbControl.survival10 = false;
            this.abbControl.survival20 = false;
        }
    }

    boolHaggle(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.haggleW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.haggle10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.haggle20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.haggleW = false;
            this.abbControl.haggle10 = false;
            this.abbControl.haggle20 = false;
        }
    }

    boolHiding(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.hidingW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.hiding10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.hiding20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.hidingW = false;
            this.abbControl.hiding10 = false;
            this.abbControl.hiding20 = false;
        }
    }

    boolRowing(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.rowingW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.rowing10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.rowing20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.rowingW = false;
            this.abbControl.rowing10 = false;
            this.abbControl.rowing20 = false;
        }
    }

    boolClimbing(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.climbingW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.climbing10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.climbing20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.climbingW = false;
            this.abbControl.climbing10 = false;
            this.abbControl.climbing20 = false;
        }
    }

    boolEvaluate(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.evaluateW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.evaluate10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.evaluate20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.evaluateW = false;
            this.abbControl.evaluate10 = false;
            this.abbControl.evaluate20 = false;
        }
    }

    boolIntimidation(tab: any) {
        if (tab[0] === true && tab[1] === false && tab[2] === false) {
            this.abbControl.intimidationW = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === false) {
            this.abbControl.intimidation10 = true;
        }
        if (tab[0] === true && tab[1] === true && tab[2] === true) {
            this.abbControl.intimidation20 = true;
        }
        if (tab[0] === false && tab[1] === false && tab[2] === false) {
            this.abbControl.intimidationW = false;
            this.abbControl.intimidation10 = false;
            this.abbControl.intimidation20 = false;
        }
    }


    /*validation_messages = {
      'name': [
        {type: 'required', message: 'Name is required.'}
      ],
      'race': [
        {type: 'required', message: 'Race is required.'}
      ],
      'age': [
        {type: 'required', message: 'Age is required.'},
      ]
    };*/

    constructor(
        public firebaseService: FirebaseService,
        private route: ActivatedRoute,
        private fb: FormBuilder,
        private router: Router,
        public dialog: MatDialog
    ) {
    }

    ngOnInit() {
        this.route.data.subscribe(routeData => {
            const data = routeData['data'];
            if (data) {
                this.item = data.payload.data();

                console.log(this.item.range1)

                this.wealth = this.item.wealth;
                this.damage = this.item.damage;

                this.command = this.item.command;
                this.gamble = this.item.gamble;
                this.ride = this.item.ride;
                this.drinking = this.item.drinking;
                this.animals = this.item.animals;
                this.gossip = this.item.gossip;
                this.swim = this.item.swim;
                this.drive = this.item.drive;
                this.charm = this.item.charm;
                this.search = this.item.search;
                this.creep = this.item.creep;
                this.perception = this.item.perception;
                this.survival = this.item.survival;
                this.haggle = this.item.haggle;
                this.hiding = this.item.hiding;
                this.rowing = this.item.rowing;
                this.climbing = this.item.climbing;
                this.evaluate = this.item.evaluate;
                this.intimidation = this.item.intimidation;

                this.item.id = data.payload.id;
                this.createForm();
            }
            this.boolCommand(this.command);
            this.boolGamble(this.gamble);
            this.boolRide(this.ride);
            this.boolDrikning(this.drinking);
            this.boolAnimals(this.animals);
            this.boolGossip(this.gossip);
            this.boolSwim(this.swim);
            this.boolDrive(this.drive);
            this.boolCharm(this.charm);
            this.boolSearch(this.search);
            this.boolCreep(this.creep);
            this.boolPerception(this.perception);
            this.boolSurvival(this.survival);
            this.boolHaggle(this.haggle);
            this.boolHiding(this.hiding);
            this.boolRowing(this.rowing);
            this.boolClimbing(this.climbing);
            this.boolEvaluate(this.evaluate);
            this.boolIntimidation(this.intimidation)


        });
        this.dataSource = new MatTableDataSource(this.wealth);
        this.damageData = new MatTableDataSource(this.damage);
        this.dataSource.paginator = this.uploadResultPaginatorWealth;
        this.damageData.paginator = this.uploadResultPaginatorDamage;
    }

    createForm() {
        this.exampleForm = this.fb.group({
            name: [this.item.name, Validators.required],
            race: [this.item.race],
            age: [this.item.age],
            game_master: [this.item.game_master],
            campaign: [this.item.campaign],
            profession: [this.item.profession],
            previous_profession: [this.item.previous_profession],
            gender: [this.item.gender],
            height: [this.item.height],
            weight: [this.item.weight],
            eyes: [this.item.eyes],
            hair: [this.item.hair],
            star_sign: [this.item.star_sign],
            marks: [this.item.marks],

            birthplace: [this.item.birthplace],
            family: [this.item.family],
            social_status: [this.item.social_status],
            past: [this.item.past],
            motivation: [this.item.motivation],
            serves: [this.item.serves],
            friends: [this.item.friends],
            foes: [this.item.foes],
            likes: [this.item.likes],
            dislikes: [this.item.dislikes],
            personality: [this.item.personality],
            goals: [this.item.goals],

            wealth: [this.item.wealth],
            damage: [this.item.damage],
            melee1: [this.item.melee1],
            melee2: [this.item.melee2],
            melee3: [this.item.melee3],
            range1: [this.item.range1],
            range2: [this.item.range2],
            range3: [this.item.range3],
            strength1: [this.item.strength1],
            strength2: [this.item.strength2],
            strength3: [this.item.strength3],
            toughness1: [this.item.toughness1],
            toughness2: [this.item.toughness2],
            toughness3: [this.item.toughness3],
            agility1: [this.item.agility1],
            agility2: [this.item.agility2],
            agility3: [this.item.agility3],
            intelligence1: [this.item.intelligence1],
            intelligence2: [this.item.intelligence2],
            intelligence3: [this.item.intelligence3],
            will_power1: [this.item.will_power1],
            will_power2: [this.item.will_power2],
            will_power3: [this.item.will_power3],
            fellowship1: [this.item.fellowship1],
            fellowship2: [this.item.fellowship2],
            fellowship3: [this.item.fellowship3],
            attacks1: [this.item.attacks1],
            attacks2: [this.item.attacks2],
            attacks3: [this.item.attacks3],
            wounds1: [this.item.wounds1],
            wounds2: [this.item.wounds2],
            wounds3: [this.item.wounds3],
            strength_bonus1: [this.item.strength_bonus1],
            strength_bonus2: [this.item.strength_bonus2],
            strength_bonus3: [this.item.strength_bonus3],
            toughness_bonus1: [this.item.toughness_bonus1],
            toughness_bonus2: [this.item.toughness_bonus2],
            toughness_bonus3: [this.item.toughness_bonus3],
            movement1: [this.item.movement1],
            movement2: [this.item.movement2],
            movement3: [this.item.movement3],
            magic1: [this.item.magic1],
            magic2: [this.item.magic2],
            magic3: [this.item.magic3],
            instability1: [this.item.instability1],
            instability2: [this.item.instability2],
            instability3: [this.item.instability3],
            fortune1: [this.item.fortune1],
            fortune2: [this.item.fortune2],
            fortune3: [this.item.fortune3],
            command: [this.item.command],
            gamble: [this.item.gamble],
            ride: [this.item.gamble],
            drinking: [this.item.drinking],
            animals: [this.item.animals],
            gossip: [this.item.gossip],
            swim: [this.item.swim],
            drive: [this.item.drive],
            charm: [this.item.charm],
            search: [this.item.search],
            creep: [this.item.creep],
            perception: [this.item.perception],
            survival: [this.item.survival],
            haggle: [this.item.haggle],
            hiding: [this.item.hiding],
            rowing: [this.item.rowing],
            climbing: [this.item.climbing],
            evaluate: [this.item.evaluate],
            intimidation: [this.item.intimidation],


            weapon1Name: [this.item.weapon1[0]],
            weapon1Dmg: [this.item.weapon1[1]],
            weapon1MinRange: [this.item.weapon1[2]],
            weapon1MaxRange: [this.item.weapon1[3]],
            weapon1Overload: [this.item.weapon1[4]],
            weapon1Quality: [this.item.weapon1[5]],
            weapon2Name: [this.item.weapon2[0]],
            weapon2Dmg: [this.item.weapon2[1]],
            weapon2MinRange: [this.item.weapon2[2]],
            weapon2MaxRange: [this.item.weapon2[3]],
            weapon2Overload: [this.item.weapon2[4]],
            weapon2Quality: [this.item.weapon2[5]],
            weapon3Name: [this.item.weapon3[0]],
            weapon3Dmg: [this.item.weapon3[1]],
            weapon3MinRange: [this.item.weapon3[2]],
            weapon3MaxRange: [this.item.weapon3[3]],
            weapon3Overload: [this.item.weapon3[4]],
            weapon3Quality: [this.item.weapon3[5]],
            weapon4Name: [this.item.weapon4[0]],
            weapon4Dmg: [this.item.weapon4[1]],
            weapon4MinRange: [this.item.weapon4[2]],
            weapon4MaxRange: [this.item.weapon4[3]],
            weapon4Overload: [this.item.weapon4[4]],
            weapon4Quality: [this.item.weapon4[5]],
            weapon5Name: [this.item.weapon5[0]],
            weapon5Dmg: [this.item.weapon5[1]],
            weapon5MinRange: [this.item.weapon5[2]],
            weapon5MaxRange: [this.item.weapon5[3]],
            weapon5Overload: [this.item.weapon5[4]],
            weapon5Quality: [this.item.weapon5[5]],

            armor1Name: [this.item.armor1[0]],
            armor1Credit: [this.item.armor1[1]],
            armor2Name: [this.item.armor2[0]],
            armor2Credit: [this.item.armor2[1]],
            armor3Name: [this.item.armor3[0]],
            armor3Credit: [this.item.armor3[1]],
            armor4Name: [this.item.armor4[0]],
            armor4Credit: [this.item.armor4[1]],
            armor5Name: [this.item.armor5[0]],
            armor5Credit: [this.item.armor5[1]],


        });
    }

    openDialog() {
        const dialogRef = this.dialog.open(AvatarDialogComponent, {
            height: '400px',
            width: '400px'
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.item.avatar = result.link;
            }
        });
    }

    onSubmit(value) {
        value.avatar = this.item.avatar;
        value.age = Number(value.age);
        this.firebaseService.updateUser(this.item.id, value)
            /*.then(
                res => {
                    this.router.navigate(['/cards']);
                }
            );*/
    }

    delete() {
        this.firebaseService.deleteUser(this.item.id)
            .then(
                res => {
                    this.router.navigate(['/cards']);
                },
                err => {
                    console.log(err);
                }
            );
    }


    addWealth(wealthValue: string, wealthInput: HTMLInputElement) {

        if (wealthValue === '') {
            console.log('empty');
        } else {
            this.wealth.push(wealthValue);
            this.dataSource._updateChangeSubscription();
        }
        wealthInput.value = '';
    }

    remove(item: Wealth): void {
        const index = this.wealth.indexOf(item);
        if (index >= 0) {
            this.wealth.splice(index, 1);
            this.dataSource = new MatTableDataSource(this.wealth);
            this.dataSource.paginator = this.uploadResultPaginatorWealth;
        }
    }

    addDamage(damageValue: string, damageInput: HTMLInputElement){

        if(damageValue == ''){
            console.log('empty')
        } else{
            console.log(damageValue)
            console.log('damageValue type: ' + typeof damageValue)
            console.log('damage type: ' + typeof this.damage)
            console.log('damage: ' + this.damage)
            this.damage.push(damageValue)
            this.damageData._updateChangeSubscription();
        }
        damageInput.value = ''
    }

    removeDamage(item: Damage) : void{
        const index = this.damage.indexOf(item);
        if(index >= 0){
            this.damage.splice(index, 1);
            this.damageData = new MatTableDataSource(this.damage);
            this.damageData.paginator = this.uploadResultPaginatorDamage;
        }
    }

    cancel() {
        this.router.navigate(['/cards']);
    }

}
