import {Injectable} from '@angular/core';
import {of as observableOf} from 'rxjs'
import {AngularFireAuth} from "@angular/fire/auth";
import {map} from "rxjs/operators";
import {auth} from 'firebase'
import * as firebase from "firebase";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private afAuth: AngularFireAuth) {
  }

  uid = this.afAuth.authState.pipe(
    map(authState => {
      if (!authState) {
        return null;
      } else {
        return authState.uid;
      }
    })
  );

  login() {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())

  }

  logout() {
    this.afAuth.auth.signOut()
  }
}
