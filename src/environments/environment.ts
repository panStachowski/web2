// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB5rekUAmGSlShTH1DWFCNaFAL_LRqYzmY",
    authDomain: "warhammer-kp.firebaseapp.com",
    databaseURL: "https://warhammer-kp.firebaseio.com",
    projectId: "warhammer-kp",
    storageBucket: "warhammer-kp.appspot.com",
    messagingSenderId: "773354415332",
    appId: "1:773354415332:web:e2d16dc6f0349926"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
